﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

using CinemaCity1.Nowy_folder;

namespace CinemaCity1.Models
{
	public class ScreeningRoomQuery
	{

		public readonly DBConnector Db;
		public ScreeningRoomQuery(DBConnector db)
		{
			Db = db;
		}

		public ScreeningRoom GetById(int id)
		{
			var result = ReadAll(FindOneCmd(id).ExecuteReader());
			return result.Count > 0 ? result[0] : null;
		}

		public async Task<ScreeningRoom> GetByIdAsync(int id)
		{
			var result = await ReadAllAsync(await FindOneCmd(id).ExecuteReaderAsync());
			return result.Count > 0 ? result[0] : null;
		}

		public List<ScreeningRoom> GetAll()
		{
			return ReadAll(GetAllCmd().ExecuteReader());
		}

		public async Task<List<ScreeningRoom>> GetAllAsync()
		{
			return await ReadAllAsync(await GetAllCmd().ExecuteReaderAsync());
		}

		private DbCommand FindOneCmd(int id)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id`, `capacity` FROM `rooms` WHERE `Id` = @id";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@id",
				DbType = DbType.Int32,
				Value = id,
			});
			return cmd as MySqlCommand;
		}

		public DbCommand GetAllCmd()
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id`, `capacity` FROM `rooms` ORDER BY `Id` DESC;";
			return cmd as MySqlCommand;
		}

		private List<ScreeningRoom> ReadAll(DbDataReader reader)
		{
			var screeningRooms = new List<ScreeningRoom>();
			using (reader)
			{
				while (reader.Read())
				{
					var screeningRoom = new ScreeningRoom(Db)
					{
						Id = reader.GetFieldValue<int>(0),
						Capacity = reader.GetFieldValue<int>(1)
					};
					screeningRooms.Add(screeningRoom);
				}
			}
			return screeningRooms;
		}

		private async Task<List<ScreeningRoom>> ReadAllAsync(DbDataReader reader)
		{
			var screeningRooms = new List<ScreeningRoom>();
			using (reader)
			{
				while (await reader.ReadAsync())
				{
					var screeningRoom = new ScreeningRoom(Db)
					{
						Id = await reader.GetFieldValueAsync<int>(0),
						Capacity = await reader.GetFieldValueAsync<int>(1)
					};
					screeningRooms.Add(screeningRoom);
				}
			}
			return screeningRooms;
		}
	}
}
