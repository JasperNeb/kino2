﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CinemaCity1.Nowy_folder;
using MySql.Data.MySqlClient;

namespace CinemaCity1
{
    // public enum Roles { user, employee, admin};

    public class User
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public String Login { get; set; }
        public String Password { get; set; }
        public String Role { get; set; }  // TODO check how to get some kind of validation on C# side

        /*public User(String name, String surname)
        {
            this.name = name;
            this.surname = surname;
        }*/

        internal DBConnector Db { get; set; }

        public User()
        {
        }

        internal User(DBConnector db)
        {
            Db = db;
        }

        public void Insert()
        {
            var cmd = InsertCmd();
            cmd.ExecuteNonQuery();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task InsertAsync()
        {
            var cmd = InsertCmd();
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public void Update()
        {
            var cmd = UpdateCmd();
            cmd.ExecuteNonQuery();
        }

        public async Task UpdateAsync()
        {
            var cmd = UpdateCmd();
            await cmd.ExecuteNonQueryAsync();
        }

        public void Delete()
        {
            var cmd = DeleteCmd();
            cmd.ExecuteNonQuery();
        }

        public async Task DeleteAsync()
        {
            var cmd = DeleteCmd();

            await cmd.ExecuteNonQueryAsync();
        }

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                MySqlDbType = MySqlDbType.Int32,
                Value = Id,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@name",
                MySqlDbType = MySqlDbType.String,
                Value = Name,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@surname",
                MySqlDbType = MySqlDbType.String,
                Value = Surname,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@login",
                MySqlDbType = MySqlDbType.String,
                Value = Login,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@password",
                MySqlDbType = MySqlDbType.String,
                Value = Password,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@role",
                MySqlDbType = MySqlDbType.String,
                Value = Role,
            });
        }

        private MySqlCommand InsertCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"INSERT INTO `BlogPost` (`Name`, `Surname`, `login`, `password`, `role`) VALUES (@title, @content);";
            BindParams(cmd);
            return cmd;
        }

        private MySqlCommand UpdateCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"UPDATE `BlogPost` SET `Name` = @name, `Surname` = @surname, `login` = @login, `password` = @password, `role` = @role WHERE `Id` = @id;";
            BindParams(cmd);
            BindId(cmd);
            return cmd;
        }

        private MySqlCommand DeleteCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"DELETE FROM `BlogPost` WHERE `Id` = @id;";
            BindId(cmd);
            return cmd;
        }
    }
}
