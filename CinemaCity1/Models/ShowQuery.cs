﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

using CinemaCity1.Nowy_folder;

namespace CinemaCity1.Models
{
	public class ShowQuery
	{

		public readonly DBConnector Db;
		public ShowQuery(DBConnector db)
		{
			Db = db;
		}

		public Show GetById(int id)
		{
			var result = ReadAll(FindOneCmd(id).ExecuteReader());
			return result.Count > 0 ? result[0] : null;
		}

		public async Task<Show> GetByIdAsync(int id)
		{
			var result = await ReadAllAsync(await FindOneCmd(id).ExecuteReaderAsync());
			return result.Count > 0 ? result[0] : null;
		}

		//TODO get by room/date?

		public List<Show> GetAll()
		{
			return ReadAll(GetAllCmd().ExecuteReader());
		}

		public async Task<List<Show>> GetAllAsync()
		{
			return await ReadAllAsync(await GetAllCmd().ExecuteReaderAsync());
		}

		public List<Show> GetAllByMovie(int movieId)
		{
			return ReadAll(GetAllByMovieCmd(movieId).ExecuteReader());
		}

		public async Task<List<Show>> GetAllByMovieAsync(int movieId)
		{
			return await ReadAllAsync(await GetAllByMovieCmd(movieId).ExecuteReaderAsync());
		}


		private DbCommand FindOneCmd(int id)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id`, `datetime`, `room_ID`, `movie_ID` FROM `shows` WHERE `Id` = @id";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@id",
				DbType = DbType.Int32,
				Value = id,
			});
			return cmd as MySqlCommand;
		}

		public DbCommand GetAllCmd()
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id`, `datetime`, `room_ID`, `movie_ID` FROM `shows` ORDER BY `Id` DESC;";
			return cmd as MySqlCommand;
		}

		public DbCommand GetAllByMovieCmd( int movieId)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id`, `datetime`, `room_ID`, `movie_ID` FROM `shows` WHERE `movie_ID` = @movie_id ORDER BY `Id` DESC;";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@movie_id",
				DbType = DbType.Int32,
				Value = movieId,
			});
			return cmd as MySqlCommand;
		}

		private List<Show> ReadAll(DbDataReader reader)
		{
			var shows = new List<Show>();
			using (reader)
			{
				while (reader.Read())
				{
					var show = new Show(Db)
					{
						Id = reader.GetFieldValue<int>(0),
						DateTime = reader.GetFieldValue<DateTime>(1),
						MovieId = reader.GetFieldValue<int>(2),
						ScreeningRoomId = reader.GetFieldValue<int>(3)
					};
					shows.Add(show);
				}
			}
			return shows;
		}

		private async Task<List<Show>> ReadAllAsync(DbDataReader reader)
		{
			var shows = new List<Show>();
			using (reader)
			{
				while (await reader.ReadAsync())
				{
					var show = new Show(Db)
					{
						Id = await reader.GetFieldValueAsync<int>(0),
						DateTime = await reader.GetFieldValueAsync<DateTime>(1),
						MovieId = await reader.GetFieldValueAsync<int>(2),
						ScreeningRoomId = await reader.GetFieldValueAsync<int>(3)
					};
					shows.Add(show);
				}
			}
			return shows;
		}
	}
}
