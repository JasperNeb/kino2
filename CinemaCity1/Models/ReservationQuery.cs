﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

using CinemaCity1.Nowy_folder;

namespace CinemaCity1.Models
{
    public class ReservationQuery
    {
        public DBConnector Db { get; }

        public ReservationQuery(DBConnector db)
        {
            Db = db;
        }

		public Reservation GetById(int id)
		{
			var result = ReadAll(FindOneCmd(id).ExecuteReader());
			return result.Count > 0 ? result[0] : null;
		}

		public async Task<Reservation> GetByIdAsync(int id)
		{
			var result = await ReadAllAsync(await FindOneCmd(id).ExecuteReaderAsync());
			return result.Count > 0 ? result[0] : null;
		}

		public List<Reservation> GetAll()
		{
			return ReadAll(GetAllCmd().ExecuteReader());
		}

		public async Task<List<Reservation>> GetAllAsync()
		{
			return await ReadAllAsync(await GetAllCmd().ExecuteReaderAsync());
		}

		public List<Reservation> GetAllByShow(int showId)
		{
			return ReadAll(GetAllByShowCmd(showId).ExecuteReader());
		}

		public async Task<List<Reservation>> GetAllByShowAsync(int showId)
		{
			return await ReadAllAsync(await GetAllByShowCmd(showId).ExecuteReaderAsync());
		}

		public void DeleteAllOldWaiting()
		{
			var txn = Db.Connection.BeginTransaction();
			try
			{
				DeleteAllOldWaitingCmd().ExecuteNonQuery();
				txn.Commit();
			}
			catch
			{
				txn.Rollback();
				throw;
			}
		}

		public async Task DeleteAllOldWaitingAsync()
		{
			var txn = await Db.Connection.BeginTransactionAsync();
			try
			{
				await DeleteAllOldWaitingCmd().ExecuteNonQueryAsync();
				await txn.CommitAsync();
			}
			catch
			{
				await txn.RollbackAsync();
				throw;
			}
		}

			private DbCommand FindOneCmd(int id)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id', 'show_id', 'user_id', 'seat', 'status', 'updated_at` FROM `reservations` WHERE `Id` = @id";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@id",
				DbType = DbType.Int32,
				Value = id,
			});
			return cmd as MySqlCommand;
		}

		public DbCommand GetAllCmd()
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id', 'show_id', 'user_id', 'seat', 'status', 'updated_at` FROM `reservations` ORDER BY `Id` DESC;";
			return cmd as MySqlCommand;
		}

		public DbCommand GetAllByShowCmd(int showId)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id', 'show_id', 'user_id', 'seat', 'status', 'updated_at` FROM `reservations`  WHERE `show_id` = @show_id ORDER BY `Id` DESC;";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@show_id",
				DbType = DbType.Int32,
				Value = showId,
			});
			return cmd as MySqlCommand;
		}

		private DbCommand DeleteAllOldWaitingCmd()
		{
			var cmd = Db.Connection.CreateCommand();
			// TODO check if this if correct?
			cmd.CommandText = @"DELETE FROM `BlogPost` WHERE `status` = @status AND TIMEDIFF(NOW(), `updated_at`) >= @time_max";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@status",
				DbType = DbType.String,
				Value = "waiting",
			});
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@time_max",
				DbType = DbType.String,
				Value = "00:03:00.000000", // 3 minutes
			});
			return cmd as MySqlCommand;
		}

		private List<Reservation> ReadAll(DbDataReader reader)
		{
			var reservations = new List<Reservation>();
			using (reader)
			{
				while (reader.Read())
				{
					var reservation = new Reservation(Db)
					{
						Id = reader.GetFieldValue<int>(0),
						UserId = reader.GetFieldValue<int>(1),
						ShowId = reader.GetFieldValue<int>(2),
						Seat = reader.GetFieldValue<int>(3),
						Status = reader.GetFieldValue<string>(4),
						UpdatedAt = reader.GetFieldValue<DateTime>(5),
					};
					reservations.Add(reservation);
				}
			}
			return reservations;
		}

		private async Task<List<Reservation>> ReadAllAsync(DbDataReader reader)
		{
			var reservations = new List<Reservation>();
			using (reader)
			{
				while (await reader.ReadAsync())
				{
					var reservation = new Reservation(Db)
					{
						Id = await reader.GetFieldValueAsync<int>(0),
						UserId = await reader.GetFieldValueAsync<int>(1),
						ShowId = await reader.GetFieldValueAsync<int>(2),
						Seat = await reader.GetFieldValueAsync<int>(3),
						Status = await reader.GetFieldValueAsync<string>(4),
						UpdatedAt = await reader.GetFieldValueAsync<DateTime>(5),
					};
					reservations.Add(reservation);
				}
			}
			return reservations;
		}
	}
}
