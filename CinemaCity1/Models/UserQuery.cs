﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

using CinemaCity1.Nowy_folder;

namespace CinemaCity1.Models
{
	public class UserQuery
	{

		public readonly DBConnector Db;
		public UserQuery(DBConnector db)
		{
			Db = db;
		}

		public User GetById(int id)
		{
			var result = ReadAll(FindOneCmd(id).ExecuteReader());
			return result.Count > 0 ? result[0] : null;
		}

		public async Task<User> GetByIdAsync(int id)
		{
			var result = await ReadAllAsync(await FindOneCmd(id).ExecuteReaderAsync());
			return result.Count > 0 ? result[0] : null;
		}

		public User GetByLogin(string login)
		{
			var result = ReadAll(FindOneByLoginCmd(login).ExecuteReader());
			return result.Count > 0 ? result[0] : null;
		}

		public async Task<User> GetByLoginAsync(string login)
		{
			var result = await ReadAllAsync(await FindOneByLoginCmd(login).ExecuteReaderAsync());
			return result.Count > 0 ? result[0] : null;
		}

		public List<User> GetAll()
		{
			return ReadAll(GetAllCmd().ExecuteReader());
		}

		public async Task<List<User>> GetAllAsync()
		{
			return await ReadAllAsync(await GetAllCmd().ExecuteReaderAsync());
		}

		public List<User> GetAllByRole(string role)
		{
			return ReadAll(GetAllByRoleCmd(role).ExecuteReader());
		}

		public async Task<List<User>> GetAllByRoleAsync(string role)
		{
			return await ReadAllAsync(await GetAllByRoleCmd(role).ExecuteReaderAsync());
		}

		private DbCommand FindOneCmd(int id)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `ID`, `Name`, `Surname`, `login`, `password`, `role` FROM `users` WHERE `Id` = @id";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@id",
				DbType = DbType.Int32,
				Value = id,
			});
			return cmd as MySqlCommand;
		}

		private DbCommand FindOneByLoginCmd(string login)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `ID`, `Name`, `Surname`, `login`, `password`, `role` FROM `users` WHERE `login` = @login";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@login",
				DbType = DbType.String,
				Value = login,
			});
			return cmd as MySqlCommand;
		}

		public DbCommand GetAllCmd()
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `ID`, `Name`, `Surname`, `login`, `password`, `role` FROM `users` ORDER BY `Id` DESC;";
			return cmd as MySqlCommand;
		}

		public DbCommand GetAllByRoleCmd(string role)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `ID`, `Name`, `Surname`, `login`, `password`, `role` FROM `users` WHERE `role` = @role ORDER BY `Id` DESC;";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@role",
				DbType = DbType.String,
				Value = role,
			});
			return cmd as MySqlCommand;
		}

		private List<User> ReadAll(DbDataReader reader)
		{
			var users = new List<User>();
			using (reader)
			{
				while (reader.Read())
				{
					var user = new User(Db)
					{
						Id = reader.GetFieldValue<int>(0),
						Name = reader.GetFieldValue<string>(1),
						Surname = reader.GetFieldValue<string>(2),
						Login = reader.GetFieldValue<string>(3),
						Password = reader.GetFieldValue<string>(4),
						Role = reader.GetFieldValue<string>(5)
					};
					users.Add(user);
				}
			}
			return users;
		}

		private async Task<List<User>> ReadAllAsync(DbDataReader reader)
		{
			var users = new List<User>();
			using (reader)
			{
				while (await reader.ReadAsync())
				{
					var user = new User(Db)
					{
						Id = await reader.GetFieldValueAsync<int>(0),
						Name = await reader.GetFieldValueAsync<string>(1),
						Surname = await reader.GetFieldValueAsync<string>(2),
						Login = await reader.GetFieldValueAsync<string>(3),
						Password = await reader.GetFieldValueAsync<string>(4),
						Role = await reader.GetFieldValueAsync<string>(5)
					};
					users.Add(user);
				}
			}
			return users;
		}
	}
}
