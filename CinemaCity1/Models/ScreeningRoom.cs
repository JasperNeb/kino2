﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CinemaCity1.Nowy_folder;
using MySql.Data.MySqlClient;

namespace CinemaCity1.Models
{
    public class ScreeningRoom
    {
        public int Id { get; set; }
        public int Capacity { get; set; }

        internal DBConnector Db { get; set; }

        public ScreeningRoom()
        {
        }

        internal ScreeningRoom(DBConnector db)
        {
            Db = db;
        }

        public void Insert()
        {
            var cmd = InsertCmd();
            cmd.ExecuteNonQuery();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task InsertAsync()
        {
            var cmd = InsertCmd();
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public void Update()
        {
            var cmd = UpdateCmd();
            cmd.ExecuteNonQuery();
        }

        public async Task UpdateAsync()
        {
            var cmd = UpdateCmd();
            await cmd.ExecuteNonQueryAsync();
        }

        public void Delete()
        {
            var cmd = DeleteCmd();
            cmd.ExecuteNonQuery();
        }

        public async Task DeleteAsync()
        {
            var cmd = DeleteCmd();

            await cmd.ExecuteNonQueryAsync();
        }

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                MySqlDbType = MySqlDbType.Int32,
                Value = Id,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@capacity",
                MySqlDbType = MySqlDbType.Int32,
                Value = Capacity,
            });
        }

        private MySqlCommand InsertCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"INSERT INTO `screeningrooms` (`movie`) VALUES (@capacity);";
            BindParams(cmd);
            return cmd;
        }

        private MySqlCommand UpdateCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"UPDATE `screeningrooms` SET `capacity` = @capacity WHERE `Id` = @id;";
            BindParams(cmd);
            BindId(cmd);
            return cmd;
        }

        private MySqlCommand DeleteCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"DELETE FROM `screeningrooms` WHERE `Id` = @id;";
            BindId(cmd);
            return cmd;
        }
    }
}
