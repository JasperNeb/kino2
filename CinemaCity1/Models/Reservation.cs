﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CinemaCity1.Nowy_folder;
using MySql.Data.MySqlClient;

namespace CinemaCity1.Models
{
    // public enum Statuses { waiting, reserved };

    public class Reservation
    {
        // TODO add seat number
        public int Id { get; set; }
        public int UserId { get; set; }
        public int ShowId { get; set; }
        public int Seat { get; set; }
        public String Status { get; set; } // TODO check how to get some kind of validation on C# side
        public DateTime UpdatedAt { get; set; } // TODO check if there is better class to store SQL's DATETIME

        internal DBConnector Db { get; set; }

        public Reservation()
        {
        }

        internal Reservation(DBConnector db)
        {
            Db = db;
        }

        public void Insert()
        {
            var cmd = InsertCmd();
            cmd.ExecuteNonQuery();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task InsertAsync()
        {
            var cmd = InsertCmd();
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public void Update()
        {
            var cmd = UpdateCmd();
            cmd.ExecuteNonQuery();
        }

        public async Task UpdateAsync()
        {
            var cmd = UpdateCmd();
            await cmd.ExecuteNonQueryAsync();
        }

        public void Delete()
        {
            var cmd = DeleteCmd();
            cmd.ExecuteNonQuery();
        }

        public async Task DeleteAsync()
        {
            var cmd = DeleteCmd();

            await cmd.ExecuteNonQueryAsync();
        }

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                MySqlDbType = MySqlDbType.Int32,
                Value = Id,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@user_id",
                MySqlDbType = MySqlDbType.Int32,
                Value = UserId,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@show_id",
                MySqlDbType = MySqlDbType.Int32,
                Value = ShowId,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@seat",
                MySqlDbType = MySqlDbType.Int32,
                Value = Seat,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@status",
                MySqlDbType = MySqlDbType.String,
                Value = Status,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@updated_at",
                MySqlDbType = MySqlDbType.DateTime,
                Value = UpdatedAt,
            });
        }

        private MySqlCommand InsertCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"INSERT INTO `reservations` (`show_id`, `user_id`, `seat`, `status`, `updated_at`) VALUES (@show_id, @user_id, @seat, @status, @updated_at);";
            BindParams(cmd);
            return cmd;
        }

        private MySqlCommand UpdateCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"UPDATE `reservations` SET `show_id` = @show_id, `user_id` = @user_id, `seat` = @seat, `status` = @status, `updated_at` = @updated_at WHERE `Id` = @id;";
            BindParams(cmd);
            BindId(cmd);
            return cmd;
        }

        private MySqlCommand DeleteCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"DELETE FROM `reservations` WHERE `Id` = @id;";
            BindId(cmd);
            return cmd;
        }
    }
}
