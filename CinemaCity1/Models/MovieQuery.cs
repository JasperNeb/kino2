﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Data;
using System.Data.Common;
using MySql.Data.MySqlClient;

using CinemaCity1.Nowy_folder;

namespace CinemaCity1.Models
{
    public class MovieQuery
    {
		public readonly DBConnector Db;
		public MovieQuery(DBConnector db)
		{
			Db = db;
		}

		public Movie GetById(int id)
		{
			var result = ReadAll(FindOneCmd(id).ExecuteReader());
			return result.Count > 0 ? result[0] : null;
		}

		public async Task<Movie> GetByIdAsync(int id)
		{
			var result = await ReadAllAsync(await FindOneCmd(id).ExecuteReaderAsync());
			return result.Count > 0 ? result[0] : null;
		}

		public Movie GetByName(String name)
		{
			var result = ReadAll(FindOneByNameCmd(name).ExecuteReader());
			return result.Count > 0 ? result[0] : null;
		}

		public async Task<Movie> GetByNameAsync(String name)
		{
			var result = await ReadAllAsync(await FindOneByNameCmd(name).ExecuteReaderAsync());
			return result.Count > 0 ? result[0] : null;
		}


		public List<Movie> GetAll()
		{
			return ReadAll(GetAllCmd().ExecuteReader());
		}

		public async Task<List<Movie>> GetAllAsync()
		{
			return await ReadAllAsync(await GetAllCmd().ExecuteReaderAsync());
		}

		private DbCommand FindOneCmd(int id)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id`, `movie` FROM `movies` WHERE `Id` = @id";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@id",
				DbType = DbType.Int32,
				Value = id,
			});
			return cmd as MySqlCommand;
		}

		private DbCommand FindOneByNameCmd(String name)
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id`, `movie` FROM `movies` WHERE `movie` = @name";
			cmd.Parameters.Add(new MySqlParameter
			{
				ParameterName = "@name",
				DbType = DbType.String,
				Value = name,
			});
			return cmd as MySqlCommand;
		}

		public DbCommand GetAllCmd()
		{
			var cmd = Db.Connection.CreateCommand();
			cmd.CommandText = @"SELECT `id`, `movie` FROM `movies` ORDER BY `Id` DESC;";
			return cmd as MySqlCommand;
		}

		private List<Movie> ReadAll(DbDataReader reader)
		{
			var movies = new List<Movie>();
			using (reader)
			{
				while (reader.Read())
				{
					var movie = new Movie(Db)
					{
						Id = reader.GetFieldValue<int>(0),
						Name = reader.GetFieldValue<string>(1)
					};
					movies.Add(movie);
				}
			}
			return movies;
		}

		private async Task<List<Movie>> ReadAllAsync(DbDataReader reader)
		{
			var movies = new List<Movie>();
			using (reader)
			{
				while (await reader.ReadAsync())
				{
					var movie = new Movie(Db)
					{
						Id = await reader.GetFieldValueAsync<int>(0),
						Name = await reader.GetFieldValueAsync<string>(1)
					};
					movies.Add(movie);
				}
			}
			return movies;
		}
	}
}
