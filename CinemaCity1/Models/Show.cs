﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CinemaCity1.Nowy_folder;
using MySql.Data.MySqlClient;

namespace CinemaCity1.Models
{
    public class Show
    {
        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public int MovieId { get; set; }
        public int ScreeningRoomId { get; set; }

        internal DBConnector Db { get; set; }

        public Show()
        {
        }

        internal Show(DBConnector db)
        {
            Db = db;
        }

        public void Insert()
        {
            var cmd = InsertCmd();
            cmd.ExecuteNonQuery();
            Id = (int)cmd.LastInsertedId;
        }

        public async Task InsertAsync()
        {
            var cmd = InsertCmd();
            await cmd.ExecuteNonQueryAsync();
            Id = (int)cmd.LastInsertedId;
        }

        public void Update()
        {
            var cmd = UpdateCmd();
            cmd.ExecuteNonQuery();
        }

        public async Task UpdateAsync()
        {
            var cmd = UpdateCmd();
            await cmd.ExecuteNonQueryAsync();
        }

        public void Delete()
        {
            var cmd = DeleteCmd();
            cmd.ExecuteNonQuery();
        }

        public async Task DeleteAsync()
        {
            var cmd = DeleteCmd();

            await cmd.ExecuteNonQueryAsync();
        }

        private void BindId(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@id",
                MySqlDbType = MySqlDbType.Int32,
                Value = Id,
            });
        }

        private void BindParams(MySqlCommand cmd)
        {
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@datetime",
                MySqlDbType = MySqlDbType.DateTime,
                Value = DateTime,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@room_id",
                MySqlDbType = MySqlDbType.Int32,
                Value = ScreeningRoomId,
            });
            cmd.Parameters.Add(new MySqlParameter
            {
                ParameterName = "@movie_id",
                MySqlDbType = MySqlDbType.Int32,
                Value = MovieId,
            });
        }

        private MySqlCommand InsertCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"INSERT INTO `shows` (`datetime`, `room_ID`, `movie_ID`) VALUES (@datetime, @room_id, @movie_id);";
            BindParams(cmd);
            return cmd;
        }

        private MySqlCommand UpdateCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"UPDATE `shows` SET `datetime` = @datetime, `room_ID` = @room_id, `movie_ID` = @movie_id WHERE `Id` = @id;";
            BindParams(cmd);
            BindId(cmd);
            return cmd;
        }

        private MySqlCommand DeleteCmd()
        {
            var cmd = Db.Connection.CreateCommand() as MySqlCommand;
            cmd.CommandText = @"DELETE FROM `shows` WHERE `Id` = @id;";
            BindId(cmd);
            return cmd;
        }
    }
}
