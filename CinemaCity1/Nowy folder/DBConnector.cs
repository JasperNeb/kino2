﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;

namespace CinemaCity1.Nowy_folder
{
    public class DBConnector :IDisposable
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public MySqlConnection Connection { get; }

        public DBConnector(string connectionString)
        {
            logger.Debug("Opening connection to the database");
            try
            {
                Connection = new MySqlConnection(connectionString);
                

            }
            catch (Exception ex)
            {
                logger.Error("Connection was NOT opened: " + ex.Message);
            }

        }

        public void Dispose() => Connection.Dispose();


    }
}
