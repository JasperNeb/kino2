﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CinemaCity1.Models;
using CinemaCity1.Nowy_folder;

namespace CinemaCity1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private static NLog.Logger loggerw;



        public DBConnector Db { get; }

        public HomeController(ILogger<HomeController> logger, DBConnector db)
        {
            Db = db;
            _logger = logger;

            loggerw = NLog.LogManager.GetCurrentClassLogger();
            Db.Connection.Open();
            using (var cmd = new MySql.Data.MySqlClient.MySqlCommand("SELECT name FROM users", Db.Connection))
            using (var reader = cmd.ExecuteReader())
            {
                reader.Read();
            }
            Models.MovieQuery mq = new Models.MovieQuery(Db);
            List<Movie> l=mq.GetAll();
            for(int i=0; i<l.Count; i++)
            {
                loggerw.Info(l[i].Name);

            }

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
